export default ({ app }) => {
 app.$axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
 app.$axios.defaults.xsrfCookieName = 'csrftoken';
}
